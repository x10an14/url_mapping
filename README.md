URL mapper
==========

This tool is meant to permit users to define URL mappings in [yaml](https://en.wikipedia.org/wiki/YAML)-format, and out said mappings in nginx-format.

(Future formats might include HAProxy?)

Pre-requisites
--------------
1. Python, version 3.7 (or greater)
2. [Pipenv](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)

How to use
----------
1. Add URL mappings/configs to `mappings.yml` in Git repo root
2. `make run`

How to install tool
-------------------
```
make deploy
```

Dev install
-----------
```
make install
```
