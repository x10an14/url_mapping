SHELL=/bin/bash
.DEFAULT_GOAL := run
python_files := $(shell find url_mapper/ -type f -name '*.py')

.PHONY: run
run: $(python_files)
	pipenv run url_mapper

.PHONY: install
install: Pipfile Pipfile.lock
	pipenv install --dev
	pipenv run pip install --editable .
	pipenv run url_mapper --help

.PHONY: check_pep8
check_pep8: $(python_files)
	pipenv run python -m flake8

.PHONY: docker
docker: Pipfile.lock mappings.yml Dockerfile docker-compose.yml
	docker-compose run url_mapper

.PHONY: lock
lock: Pipfile Pipfile.lock
	pipenv lock

.PHONY: deploy
deploy: Pipfile.lock
	pipenv install --deploy
	pipenv run pip install --editable .
