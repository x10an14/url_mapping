FROM kennethreitz/pipenv

# Update dependency management/installation tool
RUN pip install --upgrade pip

# Copy program files
COPY url_mapper url_mapper
COPY setup.* Pipenv* ./

# Install program/application
RUN pip install --editable .

# Only for example purposes
COPY mappings.yml mappings.yml

CMD url_mapper
