#!/usr/bin/env python3
from setuptools import setup, find_packages

from url_mapper import __version__


setup(
    name='url_mapper',
    version=__version__,
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'click',
        'strictyaml',
        'jinja2',
        'pipenv',
    ],
    entry_points='''
        [console_scripts]
        url_mapper=url_mapper.cli:cli
    '''
)
