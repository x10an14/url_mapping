#!/usr/bin/env python3

# Internal module package imports
from url_mapper.cli import cli


if __name__ == '__main__':
    # Just pass on to the cli-function in the
    # url_mapper/cli.py file, and let it be the 'main()' function.
    cli()
