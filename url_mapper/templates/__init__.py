# Python std lib
from pathlib import Path

# Non-standard library python package imports
from jinja2 import Environment, FileSystemLoader


TEMPLATE_DIRECTORY = (
    Path(__file__) / '..' / '..' / 'templates'
).resolve(strict=True)
assert(
    TEMPLATE_DIRECTORY.is_dir()
    and TEMPLATE_DIRECTORY.name == 'templates'
)
FILE_LOADER = FileSystemLoader(str(TEMPLATE_DIRECTORY))
JINJA_ENV = Environment(loader=FILE_LOADER)
JINJA_ENV.trim_blocks = True
