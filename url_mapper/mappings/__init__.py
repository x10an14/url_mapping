# Python std lib
from typing import Dict, Generator, Any

# Non-standard library python package imports
from strictyaml import YAML

# Internal module imports
from url_mapper.mappings.mapping import Mapping     # noqa
from url_mapper.mappings.redirect import Redirect   # noqa
from url_mapper.mappings.reverse_proxy import ReverseProxy     # noqa


__all__ = (
    # Base class
    'Mapping'

    # Child classes in use:
    'ReverseProxy', 'Redirect',

    # Functions inside of this file:
    'parse_mapping',

    # Variables  inside of this file:
    '__class_dict'
)


__class_dict = {
    Redirect: vars(Redirect)['__dataclass_fields__'].keys(),
    ReverseProxy: vars(ReverseProxy)['__dataclass_fields__'].keys(),
}


def parse_mapping(
    MappingCls,
    **mappings: Dict[str, YAML],
) -> Generator[Any, None, None]:
    f"""
    Parse one or more key/value pairs of types: {{'external_endpoint': StrictYaml}};

    StrictYaml containing keys: {__class_dict[MappingCls]}

    Return a list of {MappingCls.__name__} per 'extenal_endpoint'
    """
    for external_endpoint, config in mappings.items():
        external_endpoint = str(external_endpoint)  # Assert non-empty string

        endpoint_config = {
            str(k): v
            for k, v in config.data.items()
            if k in __class_dict[MappingCls]
        }
        endpoints = [external_endpoint] + config.data.get(
            'identical_endpoints', list()
        )
        for endpoint in endpoints:
            yield MappingCls(
                external_endpoint=endpoint,
                yaml_source=config.as_yaml(),
                starting_line_no=config.start_line,
                **endpoint_config,
            )
