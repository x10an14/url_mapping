"""Module implementing Nginx `return <http redirect code>` locations"""
# Python standard library imports
from dataclasses import field, dataclass

# Internal module package imports
from url_mapper.mappings.mapping import Mapping
from url_mapper.templates import JINJA_ENV


@dataclass()
class Redirect(Mapping):
    """docstring for ReverseProxy"""
    redirect_http_code: int = field(default=302)

    def __post_init__(self):
        """Validate Mapping config"""
        is_validated = self.__verify_webserver_redirect()
        self.valid = is_validated

    def __verify_webserver_redirect(self) -> bool:
        """Check if http code is in a valid range"""
        http_code = int(self.redirect_http_code)
        return (http_code >= 300 and http_code < 400)

    def create_location(self) -> str:
        """
            Wrapper/ease-of-life function for converting a Mapping to an nginx location block (as string).
        """
        # convert mapping variable to dictionary:
        mapping = vars(self)

        mapping['comment'] = '' if mapping['valid'] else '#'
        template = JINJA_ENV.get_template(
            'nginx_location_redirect.template'
        )
        return template.render(**mapping) + '\n'
