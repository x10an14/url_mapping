# Python standard library imports
from dataclasses import dataclass, field
from enum import Enum
from typing import Dict, Tuple, Union


class MatchRule(Enum):
    """Nginx MatchRules for location"""
    STRING_MATCH = 1
    EXACT_MATCH = 2
    CASE_SENSITIVE_REGEX = 3
    CASE_INSENSITIVE_REGEX = 4
    NO_REGEXP_MATCH = 5

    @staticmethod
    def create_rule(rule: str = 'string_match'):
        """Class method for generating instances of class via strings"""
        existing_rules = {
            rule.lower(): value
            for rule, value in vars(MatchRule).items()
            if not (rule.startswith('_') or rule == 'create_rule')
        }
        if rule.lower() not in existing_rules:
            raise NotImplementedError
        return existing_rules[rule.lower()]


MappingConfig = Dict[
    str,
    Union[
        str,
        Union[
            int,
            Union[
                bool,
                Union[
                    MatchRule,
                    Tuple[
                        Dict[
                            str,
                            str
                        ]
                    ]
                ]
            ]
        ]
    ]
]


@dataclass()
class Mapping:
    """'Main' ancestral class for types of mappings"""
    external_endpoint: str
    yaml_source: str
    starting_line_no: int
    internal_endpoint: str
    headers: Dict[str, str] = field(default_factory=dict)
    match_rule: MatchRule = field(default_factory=MatchRule.create_rule)
    valid: bool = field(default=False, compare=False, init=False)

    def __post_init__(self):
        """Validate Mapping config"""
        is_validated = (
            # self.__verify_webserver_redirect()
            self.__verfify_rule_match()
            and self.__verify_endpoints()
        )
        self.valid = is_validated

    def __verify_endpoints(self) -> bool:
        """Ensure valid endpoint schemas"""
        # Todo: Not yet implemented
        return True

    def __verfify_rule_match(self) -> bool:
        # TODO:
        return True
