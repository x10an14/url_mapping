"""Module implementing Nginx `proxy_pass` locations"""
# Python standard library imports
from dataclasses import dataclass

# Internal module package imports
from url_mapper.mappings.mapping import Mapping
from url_mapper.templates import JINJA_ENV


@dataclass()
class ReverseProxy(Mapping):
    """docstring for ReverseProxy"""

    def create_location(self) -> str:
        """
            Wrapper/ease-of-life function for converting a Mapping to an nginx location block (as string).
        """
        # convert mapping variable to dictionary:
        mapping = vars(self)

        # Field not supposed to be found in yaml
        # should only exist in _parsed_ yaml...
        assert('valid' in mapping)

        mapping['comment'] = '' if mapping['valid'] else '#'
        template = JINJA_ENV.get_template(
            'nginx_location_proxy_pass.template'
        )
        return template.render(**mapping) + '\n'
