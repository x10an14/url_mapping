# Python standard library imports
# from json import dumps
from pathlib import Path
from socket import getfqdn

# Non-standard library python package imports
from click import Path as click_Path
from strictyaml import load, YAML
from click import (
    command,
    pass_context,
    echo,
    option,
    version_option,
)

# Internal module package imports
from url_mapper.mappings import (
    parse_mapping,
    ReverseProxy,
    Redirect,
)
from url_mapper import __version__


def load_yaml(
    path: Path,
    *,
    ignore_keys=None,
) -> YAML:
    if not isinstance(path, Path):
        path = Path(str(path))
    if not ignore_keys:
        # Don't output the example in the yml file
        ignore_keys = ('examples', )

    yaml_input = load(
        path.resolve(strict=True).read_text()
    )

    for key in ignore_keys:
        if key in yaml_input.keys():
            del yaml_input[key]
    return yaml_input


@command()
@option(
    '--config-file', '-c',
    default=Path('mappings.yml'),
    type=click_Path(exists=True),
    show_default=True,
)
@option(
    '--reverse-proxies', 'reverse_proxy_flag',
    is_flag=True, show_default=True, default=True,
)
@option(
    '--redirects', '-r', 'redirects_flag',
    is_flag=True, show_default=True, default=True,
)
@option(
    '--try-files', '-t', 'try_files_flag',
    is_flag=True, show_default=True, default=False,
)
@version_option(version=__version__)
@pass_context
def cli(
    ctx,
    config_file: Path,
    reverse_proxy_flag: bool,
    redirects_flag: bool,
    try_files_flag: bool,
):
    config_file = config_file.resolve()

    # Remove non-serializable irrelevant-to-the-enduser variable
    # from locals()
    inputs = locals()
    del inputs['ctx']

    # Make Path variable serializable by turning it into a String
    inputs['config_file'] = str(config_file)
    # echo(
    #     f"'{config_file.parent}' invoked with:\n{dumps(inputs, indent=2)}\n"
    # )

    results, yaml_file = list(), load_yaml(config_file)
    if reverse_proxy_flag:
        for endpoint, yaml_config in yaml_file['proxy_pass'].items():
            results += [
                mapping.create_location()
                for mapping in parse_mapping(
                    ReverseProxy,
                    **{endpoint.text: yaml_config}
                )
            ]

    if redirects_flag:
        for endpoint, yaml_config in yaml_file['redirect'].items():
            results += [
                mapping.create_location()
                for mapping in parse_mapping(
                    Redirect,
                    **{endpoint.text: yaml_config}
                )
            ]

    if try_files_flag:
        hostname = getfqdn().split('.', maxsplit=1)[0]
        raise NotImplementedError('--try-files not yet implemented.')
        for endpoint, yaml_config in yaml_file['try_files'].items():
            pass

    for output in sorted(results):
        echo(message=output)
